/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.sap.productconfig.frontend.constants;

/**
 * Global class for all ysapproductconfigaddon web constants. You can add global constants for your extension into
 * this class.
 */
public final class SapproductconfigfrontendWebConstants
{
	public static final String CONFIG_OVERVIEW_URL = "/configOverview";
	public static final String CONFIG_URL = "/config";

	private SapproductconfigfrontendWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
