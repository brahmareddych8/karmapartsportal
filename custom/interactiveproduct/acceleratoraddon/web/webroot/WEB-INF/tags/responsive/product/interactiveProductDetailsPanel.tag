<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link rel="stylesheet" type="text/css" href="${contextPath}/_ui/addons/interactiveproduct/responsive/common/css/interactiveproduct.css">


<spring:htmlEscape defaultHtmlEscape="true" />
<div class="product-details page-title">
	 <ycommerce:testId
		code="productDetails_productNamePrice_label_${product.code}">
		<div class="name">${product.name}
			<%-- <span class="sku">ID</span><span class="code">${product.code}</span> --%>
		</div>
	</ycommerce:testId>
	<%--<product:productReviewSummary product="${product}" showLinks="true" /> --%>

</div>
<div class="col-sm-12">
	<hr>
</div>
<div class="row">
	
	<div class="col-md-12">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div id="interactiveImage" ></div>
			</div>
		</div>
		<div class="col-md-5">			
			<div id="productPurchase"></div>
			
		</div>


	</div>

</div>
<script type='text/javascript' src="${contextPath}/_ui/addons/interactiveproduct/responsive/common/js/d3.min.js"></script>
<script type='text/javascript' src="${contextPath}/_ui/addons/interactiveproduct/responsive/common/js/SVGInterActUtil.js"></script>
<script type='text/javascript' src="${contextPath}/_ui/addons/interactiveproduct/responsive/common/js/lodash.min.js"></script>
<script type="application/javascript" >
	<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
   		SVGInterActUtil.getInteractiveImage('${container.product.url}');
   	</c:forEach>
</script>
<script type="text/javascript">
    var productData = [];
    <c:forEach items="${categoryProducts}" var="container" varStatus="varStatus">
    <spring:url value="${container.url}" var="prdUrl" htmlEscape="false"/> 
 	  <c:set var="prdImageUrl" value="${container.images[2].url}" />
  		<c:choose>
	    		<c:when test="${fn:length(container.customEntries) > 0}">
		    		<c:forEach items="${container.customEntries}" var="entry">
		    		<c:choose>
		    			<c:when test="${entry.categoryCode eq fn:substringAfter(product.code, 'K')}">
		    			var part = {
		    					"annotation": "${entry.annotation}",
		    					"code":       "${container.code}",
		    					"name":		  "<c:out value='${container.name}'/>",
		    					"minQuantity":"${entry.minQty}",
		    					"url":        "${prdUrl}",
		    					"imageUrl":   "${prdImageUrl}",
		    					"comment":   "<c:out value='${entry.comment}'/>"
		    					};				     
		    			productData.push(part);
		    			</c:when>
		    		</c:choose>
		          </c:forEach>
	    		</c:when>
	    		<c:otherwise>
	    		var part = {
	    				"annotation": "${container.partIndex}",
	    				"code":       "${container.code}",
	    				"name":		  "<c:out value='${container.name}'/>",
	    				"minQuantity":"${container.minQty}",
	    				"url":        "${prdUrl}",
	    				"imageUrl":   "${prdImageUrl}",
    					"comment":   "<c:out value='${container.comment}'/>'"
	    				};				     
	    		productData.push(part);       
  			</c:otherwise>
    		</c:choose>
		 
  </c:forEach>
    SVGInterActUtil.ProductData = productData;
</script>
 <%-- 
 <c:forEach items="${categoryProducts}" var="container" varStatus="varStatus">
 	<spring:url value="${container.url}" var="searchUrl" htmlEscape="false"/>
	<a href="${searchUrl}"><strong id="product">${container.code} : ${container.name}</strong></a><br/>
 </c:forEach> 
 --%>