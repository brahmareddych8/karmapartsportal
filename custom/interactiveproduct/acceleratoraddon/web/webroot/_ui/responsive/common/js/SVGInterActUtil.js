/* This is utility for Interactive SVG */
/*
 There are pre requisite
 1. Un-group ordinal Image with IncScape Tool - Manual IncScape
 2. Regroup and Stamp IDs with Parts based on Parts Spreadsheet. - Manual IncScape [Naming Conventions ID="Info_1_Part_1" for part 1]
 3. Optimize and Remove unwanted groups - SVGO Tool (Command = svgo --disable=cleanupIDs sample.svg)
 4. Add Info Icons in front of part numbers - Manual IncScape [Naming Conventions ID="Info_1" for part 1]
 5. Again Optimize and Remove unwanted groups - SVGO Tool (Command = svgo --disable=cleanupIDs sample.svg)
 6. Map Json Data with Part Number and Product Code.
 */

/*
 *
 * Javascript library
 * Dependency D3 Javascript library
 * @sureshs
 */

(function(SVGInterActUtil) {

	// create sub packages
	if (!SVGInterActUtil)
		SVGInterActUtil = {};
	SVGInterActUtil.version = '1.0';
	SVGInterActUtil.HOVER_STOKE_COLOR = '#428BCA';
	SVGInterActUtil.ProductData = [];

	SVGInterActUtil.getInteractiveImage = function(imageUrl, strokeColor) {
		if (!strokeColor) {
			SVGInterActUtil.HOVER_STOKE_COLOR = '#428BCA';
		} else {
			SVGInterActUtil.HOVER_STOKE_COLOR = strokeColor;
		}
		setTimeout(function() {
			d3.xml(imageUrl).mimeType("image/svg+xml").get(
					function(error, xml) {
						if (error)
							throw error;
						d3.select("#interactiveImage").node().appendChild(
								xml.documentElement);
						SVGInterActUtil.initEventHandlers();
					});
		}, 1000);
	};

	SVGInterActUtil.initEventHandlers = function() {
		var elements = document.querySelectorAll('svg g');

		$("#bound-path").attr('style', 'cursor:pointer');

		for (var i = 0; i < elements.length; ++i) {
			//elements[i].setAttribute('fill','transparent');
			if (elements[i].getAttribute('id') == 'bound-path') {
				elements[i].setAttribute('fill', 'transparent');
			}
			elements[i].setAttribute('cursor', 'pointer');
			elements[i].addEventListener("mouseover",
					SVGInterActUtil.showHoverPartDetails);
			elements[i].addEventListener("mouseout",
					SVGInterActUtil.hidePartDetails);
			elements[i].addEventListener("mousedown",
					SVGInterActUtil.showPartDetails);
		}
		bindInfoBox();

	};

	function bindInfoBox() {
		$("g[id*='info']").each(function(index) {
			$(this).attr('title', '');
			$(this).attr('data-content', '');
			$(this).popover({
				'trigger' : 'hover',
				'container' : 'body',
				'placement' : 'auto top',
				'white-space' : 'nowrap',
				'html' : 'true'
			});
		});
		$("g[id*='info'] g").each(function(index) {
			$(this).attr('fill', '#3E75B2');
		});

	}

	SVGInterActUtil.showHoverPartDetails = function(evt) {
		var elem = evt.currentTarget;
		var boundBox = elem.getAttribute('bound');
		if (elem && elem.nodeName === 'svg') {
			return;
		} else {
			var prdCode = elem.getAttribute('id');
			console.log('ID Hover - ' + prdCode);

			if (prdCode && (prdCode.indexOf("info") >= 0)) {
				console.log('PRD Code Hover - ' + prdCode);
				SVGInterActUtil.showToolTip(prdCode);
			}
			else if(boundBox && boundBox === 'kit_bound'){

		        var partList = elem.getAttribute('partList');

		        if(partList && partList.split(',').length > 0){

		          var parts = partList.split(',');

		          $.each( $("svg g[id*='part']"), function( index, value ){
		            $.each(parts, function( index, part ){
		              if(value.getAttribute('id') == part){
		                var pathsObj = value.querySelectorAll('path');
		                for(var i=0; i<pathsObj.length; ++i) {
		                  pathsObj[i].style.stroke  = SVGInterActUtil.HOVER_STOKE_COLOR;
		                  pathsObj[i].setAttribute('stroke-linecap','round');
		                }
		              }
		            })
		          });

		        }

		      }
			

			try {
				// Info Icon Hover
				var paths = elem.querySelectorAll('g circle');
				for (var i = 0; i < paths.length; ++i) {
					paths[i].style.stroke = SVGInterActUtil.HOVER_STOKE_COLOR;
					paths[i].setAttribute('stroke-linecap', 'round');
					// SVGInterActUtil.showToolTip(paths[i]);
				}

				// Part Group Hover Hover
				var paths = elem.querySelectorAll('path');
				for (var i = 0; i < paths.length; ++i) {
					paths[i].style.stroke = SVGInterActUtil.HOVER_STOKE_COLOR;
					paths[i].setAttribute('stroke-linecap', 'round');
				}

			} catch (err) {
				// Handle error(s) here
			}

		}

	}

	SVGInterActUtil.hidePartDetails = function(evt) {
		var elem = evt.currentTarget;
		var boundBox = elem.getAttribute('bound');
		
		// console.log('ID Out - '+ elem.getAttribute('id'));
		elem.setAttribute('stroke-linecap', 'square');
		
		var prd = elem.getAttribute('id');

	    if(boundBox && boundBox === 'kit_bound') {
	      console.log('ID Hover g-bound - ' + boundBox);
	      $.each($("svg g[id*='part']"), function (index, value) {
	        var pathsObj = value.querySelectorAll('path');
	        for(var i=0; i<pathsObj.length; ++i) {
	          pathsObj[i].style.stroke  = "#000";
	          pathsObj[i].setAttribute('stroke-linecap','round');
	        }
	      });
	    }

		try {
			var paths = elem.querySelectorAll('g');
			for (var i = 0; i < paths.length; ++i) {
				paths[i].style.stroke = '#000000';
				// $(paths[i]).tooltip('hide');
			}
			paths = elem.querySelectorAll('path');
			for (var i = 0; i < paths.length; ++i) {
				paths[i].style.stroke = '#000000';
			}
		} catch (err) {
			console
					.log('There seems to be an exception for an Interactive SVG');
		}
	};

	SVGInterActUtil.showToolTip = function(prdCode) {
		if (prdCode && (prdCode.lastIndexOf("_") >= 0)) {
			var annotationStartIndex = prdCode.lastIndexOf("_");
			var strAnnotation = prdCode.substring(annotationStartIndex + 1);
			var partList = _.filter(SVGInterActUtil.ProductData, function(o) {
				return (o.annotation === strAnnotation);
			});
			if (partList && partList.length > 0) {
				var popOverBody = "";
				var PartNumber = "";
				for (var i = 0; i < partList.length; i++) {
					var part = partList[i];
					PartNumber = 'Product Details (' + part.annotation + ')';
					popOverBody = popOverBody
							+ '<p><span><strong>Description :</strong> '
							+ part.name + '</span></p>'
							+ '<p><span><strong>Item Code:</strong> '
							+ part.code + '</span></p>'
							+ '<p><span><strong>Minimum Quantity:</strong> '
							+ part.minQuantity + '</span></p>';
					if (partList.length > 1) {
						popOverBody = popOverBody + '<hr>';
					}
				}
				;

				$('#' + prdCode).attr('data-original-title', PartNumber);
				$('#' + prdCode).attr('data-content', popOverBody);
			}
		}
	};

	SVGInterActUtil.showPartDetails = function(evt) {
		var elem = evt.currentTarget;
		var prdCode = elem.getAttribute('id');
		if (prdCode && (prdCode.indexOf("part") >= 0)) {
			try {
				SVGInterActUtil.makeHyCall(prdCode);
			} catch (err) {
				console
						.log('There seems to be an exception while getting Part Details');
			}
		}
	};

	SVGInterActUtil.makeHyCall = function(prdCode) {
        jQuery.ajaxSetup({async:false});
		var annotationStartIndex = prdCode.lastIndexOf("_");
		var catCode = prdCode.substring(0, prdCode.indexOf("_"));
		var strAnnotation = prdCode.substring(annotationStartIndex + 1);
		var partList = _.filter(SVGInterActUtil.ProductData, function(o) {
			return (o.annotation === strAnnotation);
		});
		if (partList && partList.length > 0) {

			$('#productPurchase').html('');
			var flagShowCombine;
			if (partList.length > 1) {
				flagShowCombine = true;
			} else {
				flagShowCombine = false;
			}

			for (var i = 0; i < partList.length; i++) {
				var part = partList[i];
				//IGNORE PRODUCTS STARTING WITH K AND ANNOTATION SAME AS INTERACTIVE PRODUCT
				if (part.code.indexOf("K") == 0)
					continue;
				var blocUICSS = {
					padding : 0,
					margin : 0,
					width : '100px',
					top : '40%',
					left : '50%',
					textAlign : 'center',
					color : '#000',
					border : '0px solid #aaa',
					backgroundColor : '#ccc',
					cursor : 'wait',
					margin : '0px auto 0px -50px',
					'border-radius' : '10px'
				};
				try {
					$.blockUI({
						message : '<span class="processing-blockui"></span>',
						css : blocUICSS
					});
					var url = part.url + "/p/purchase/" + catCode + "/"
							+ strAnnotation + "/" + part.code;
					$.get(url, function(response) {
						resultObj = response;
						$.unblockUI();
						if (flagShowCombine) {
							$('#productPurchase').append(resultObj);
						} else {
							$('#productPurchase').html(resultObj);
						}

					});
				} catch (e) {
					$.unblockUI();
					console.log('Error in product page');
				}
			}
// Check for dealer price and disable Add to cart button if no dealer price found
			SVGInterActUtil.CheckPriceAvailible(partList);
		}

	};

 SVGInterActUtil.CheckPriceAvailible = function(partList) {
	 jQuery.ajaxSetup({async:false});
			for (var i = 0; i < partList.length; i++) {
				var part = partList[i];
				console.log("Check price availability before " + part.code);
				
					  var partUrl =  part.url + "/p/dealer/" + part.code;
						$.get(partUrl, function(result) {
							var dealerPrice = result;
							
						//based on response //Disable the button/enable button
						var part_str = part.code;
						var str_form = "_Form";
						var str_add_to_cart = " #addToCartButton";
						var str_hash = "#";
						var str_res = str_hash.concat(part_str,str_form,str_add_to_cart);
		 
						if(dealerPrice == "false")
						{ 
							console.log("str_res " + $("#" + part.code + "_Form #addToCartButton").length);
							console.log(str_res);
								//$("#" + part.code + "_Form #addToCartButton").prop("disabled", true);
								$(str_res).prop("disabled", true);
								$("#h2 #"+part.code+"_nopriceavailable").html("Note: Contact Karma Associate");
								console.log("Disabled");
							
						}
					   else 
						{
							$(str_res).removeAttr('disabled');  
							$("#h2 #"+part.code+"_nopriceavailable").html("");
						}			
						});	
				
			}
			
 }	


 }(window.SVGInterActUtil = window.SVGInterActUtil || {}));