/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.interactiveproduct.constants;

/**
 * Global class for all Interactiveproduct constants. You can add global constants for your extension into this class.
 */
public final class InteractiveproductConstants extends GeneratedInteractiveproductConstants
{
	public static final String EXTENSIONNAME = "interactiveproduct";
	public static final String VIEW_PAGE_PREFIX = "addon:/interactiveproduct";

	private InteractiveproductConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
